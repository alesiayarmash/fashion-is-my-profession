//
// Created by Pavel Kasila on 7.05.24.
//

#ifndef CREATIVE_STATISTICS_MODE_H
#define CREATIVE_STATISTICS_MODE_H

#include <cstdint>

namespace outfit::core::statistics {
enum class StatisticsMode : uint8_t {
    Categories = 0,
    Materials = 1,
    Sizes = 2,
    Seasons = 3,
};
}  // namespace outfit::core::statistics

#endif  // CREATIVE_STATISTICS_MODE_H
